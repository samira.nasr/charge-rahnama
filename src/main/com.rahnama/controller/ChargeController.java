package com.rahnama.controller;

import com.rahnama.bo.ChargeBO;
import com.rahnama.bo.PaymentBO;
import com.rahnama.model.Charge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.rahnama.service.ChargeService;


/**
 * Created by rahnema on 7/11/2017.
 */
@RestController
@RequestMapping("/")
public class ChargeController {
    private static final Logger logger = LoggerFactory.getLogger(ChargeController.class);

    @Autowired
    private ChargeService chargeService;

    @RequestMapping(value = "charge",method = RequestMethod.POST)
    public String charge(@RequestBody Charge charge){
        logger.info("request for charge");
        return "charge";
    }

    @RequestMapping(value = "submit",method = RequestMethod.POST)
    public @ResponseBody PaymentBO submitCharge(
            @RequestBody ChargeBO charge) {
        logger.info("charge request submit");
        try {
            return chargeService.charge(new ChargeBO(charge.getPhoneNumber(),charge.getOperatorStr(),charge.getAmount(),charge.getRequestNumber()));
        }finally {

        }
//        }catch (Exception e){
//            //return "redirect:/unsuccessfulCharge";
//        }

    }

}
