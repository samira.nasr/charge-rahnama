package com.rahnama.service;

import com.rahnama.bo.ChargeBO;
import com.rahnama.bo.PaymentBO;
import com.rahnama.dao.ChargeDAO;
import com.rahnama.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by rahnema on 7/11/2017.
 */
@Service
public class ChargeService  {
    @Autowired
    PaymentService paymentService;

    @Autowired
    ChargeDAO chargeDAO;

    public PaymentBO charge(ChargeBO charge) throws RuntimeException{
        chargeDAO.save(charge.getCharge());
        return paymentService.payment(charge);
    }
}
