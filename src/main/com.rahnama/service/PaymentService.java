package com.rahnama.service;

import com.rahnama.bo.ChargeBO;
import com.rahnama.bo.PaymentBO;
import com.rahnama.model.Charge;
import com.rahnama.operator.PSP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by rahnema on 7/11/2017.
 */
@Service
public class PaymentService {
    @Autowired
    PSP psp;

    public PaymentBO payment(ChargeBO charge) throws RuntimeException {
        return psp.payment(charge);
    }
}
