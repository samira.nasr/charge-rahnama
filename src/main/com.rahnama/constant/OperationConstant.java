package com.rahnama.constant;

import com.rahnama.model.OperationLog;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rahnema on 7/11/2017.
 */
public class OperationConstant {
    public static Map<String , String > operationLogMap = new HashMap<String,String>();
    static {
        operationLogMap.put(OperationLog.PAYMENT_DONE.getOperationLogCode(), "payment done");
        operationLogMap.put(OperationLog.PATMENT_ERROR.getOperationLogCode(), "payment face an error and not done!");
        operationLogMap.put(OperationLog.CONFIRM_SUCCESS.getOperationLogCode(), "Charge Confirmed");
        operationLogMap.put(OperationLog.CONFIRM_ERROR.getOperationLogCode(), "Charge Confirm has error");
    }

}
