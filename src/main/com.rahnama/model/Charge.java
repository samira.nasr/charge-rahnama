package com.rahnama.model;


import java.io.Serializable;

/**
 * Created by rahnema on 7/11/2017.
 */
public class Charge implements Serializable {
    private String phoneNumber;
    private String operator;
    private String amount;
    private Long id;


    public Charge() {
    }

    public Charge(String phoneNumber, String operator, String amount) {
        this.phoneNumber = phoneNumber;
        this.operator = operator;
        this.amount = amount;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
