package com.rahnama.model;

import java.io.Serializable;

/**
 * Created by rahnema on 7/11/2017.
 */
public class PaymentTrx implements Serializable {
    private Long id;
    private Charge charge;
    private String trxNumber;

    public String getTrxNumber() {
        return trxNumber;
    }

    public void setTrxNumber(String trxNumber) {
        this.trxNumber = trxNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Charge getCharge() {
        return charge;
    }

    public void setCharge(Charge charge) {
        this.charge = charge;
    }
}
