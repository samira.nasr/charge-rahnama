package com.rahnama.model;

/**
 * Created by rahnema on 7/11/2017.
 */
public class LogDB {
    private String operation;
    private Charge charge;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Charge getCharge() {
        return charge;
    }

    public void setCharge(Charge cherge) {
        this.charge = cherge;
    }
}
