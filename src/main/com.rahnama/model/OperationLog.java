package com.rahnama.model;

/**
 * Created by rahnema on 7/11/2017.
 */
public enum OperationLog {
    PAYMENT_DONE("PAYMENT_DONE"),
    PATMENT_ERROR("PAYMENT_ERROR"),
    CONFIRM_SUCCESS("CONFIRM_SUCCESS"),
    CONFIRM_ERROR("CONFIRM_ERROR"),
    REVERSE_SUCCESS("REVERSE_SUCCESS"),
    REVERSE_ERROR("REVERSE_ERROR");

    private final String operationLogCode;

    OperationLog(String operationLogCode) {
        this.operationLogCode = operationLogCode;
    }

    public String getOperationLogCode() {
        return operationLogCode;
    }
}

