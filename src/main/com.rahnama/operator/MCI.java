package com.rahnama.operator;

import com.rahnama.bo.ChargeBO;
import com.rahnama.bo.PaymentBO;
import com.rahnama.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by rahnema on 7/11/2017.
 */
public class MCI implements Operator {
    String name;
    public MCI() {
        this.name = "MCI";
    }

    @Autowired
    PSP psp;

    public PaymentBO topUP(String phone, String amount, String requestNumber) {
        ChargeBO chargeBO = new ChargeBO(phone,this.name,amount,requestNumber);
        try {
            return psp.confirm(chargeBO);
        }catch(RuntimeException e){
            return psp.reverse(chargeBO);
        }
    }
}
