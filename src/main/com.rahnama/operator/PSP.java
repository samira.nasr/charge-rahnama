package com.rahnama.operator;

import com.rahnama.bo.ChargeBO;
import com.rahnama.bo.PaymentBO;
import com.rahnama.constant.OperationConstant;
import com.rahnama.dao.LogDAO;
import com.rahnama.model.Charge;
import com.rahnama.model.LogDB;
import com.rahnama.model.OperationLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Created by rahnema on 7/11/2017.
 */
@Service
@Configurable
public class PSP {
    private static final Logger logger = LoggerFactory.getLogger(PSP.class);
    @Autowired
    LogDAO logDAO;

    @Autowired
    private ApplicationContext context;

    public PaymentBO payment(ChargeBO charge) throws RuntimeException{
        logger.info("call payment wrapper");
        LogDB logDB = new LogDB();
        logDB.setCharge(charge.getCharge());
        try {
           PaymentBO paymentBO;
           // todo : call payment method from outer interface and set result in paymentBO

            Operator operator=null;
            if(charge.getOperatorStr().equalsIgnoreCase("MTN")){
                operator = (Operator) context.getBean("MTNIrancell");
            }
            paymentBO = operator.topUP(charge.getPhoneNumber(),charge.getAmount(),charge.getRequestNumber());
            paymentBO.setRequestNumber(charge.getRequestNumber());

           logDB.setOperation(OperationLog.PAYMENT_DONE.getOperationLogCode());
           System.out.println(OperationConstant.operationLogMap.get(OperationLog.PAYMENT_DONE.getOperationLogCode()));
           paymentBO.setProcessMsg(OperationConstant.operationLogMap.get(OperationLog.PAYMENT_DONE.getOperationLogCode()));
           return paymentBO;
       }catch (Exception e){
            logDB.setOperation(OperationLog.PATMENT_ERROR.getOperationLogCode());
            System.out.println(OperationConstant.operationLogMap.get(OperationLog.PATMENT_ERROR.getOperationLogCode()));
            throw new RuntimeException(OperationConstant.operationLogMap.get(OperationLog.PATMENT_ERROR.getOperationLogCode()));//todo : could use our exception instead of runtimeException
       }finally {
            logDAO.save(logDB);
        }
    }

    public PaymentBO confirm(ChargeBO charge){
        logger.info("call confirm wrapper");
        LogDB logDB = new LogDB();
        logDB.setCharge(charge.getCharge());
        try {
            // todo : call confirm method from outer interface and set result in paymentBO
            PaymentBO paymentBO = new PaymentBO();
            paymentBO.setRequestNumber(charge.getRequestNumber());

            logDB.setOperation(OperationLog.CONFIRM_SUCCESS.getOperationLogCode());
            System.out.println(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_SUCCESS.getOperationLogCode()));
            paymentBO.setProcessMsg(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_SUCCESS.getOperationLogCode()));

            return paymentBO;
        }catch (Exception e){
            logDB.setOperation(OperationLog.CONFIRM_ERROR.getOperationLogCode());
            System.out.println(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_ERROR.getOperationLogCode()));
            throw new RuntimeException(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_ERROR.getOperationLogCode()));
        }finally {
            //logDAO.save(logDB);
        }
    }

    public PaymentBO reverse(ChargeBO charge){
        logger.info("call reverse wrapper");
        LogDB logDB = new LogDB();
        logDB.setCharge(charge.getCharge());
        try {
            // todo : call reverse method from outer interface and set result in paymentBO
            PaymentBO paymentBO = new PaymentBO();
            paymentBO.setRequestNumber(charge.getRequestNumber());

            logDB.setOperation(OperationLog.CONFIRM_SUCCESS.getOperationLogCode());
            System.out.println(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_SUCCESS.getOperationLogCode()));
            paymentBO.setProcessMsg(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_SUCCESS.getOperationLogCode()));

            return paymentBO;
        }catch (Exception e){
            //todo : maybe in bussiness reverse should call until it become successful
            logDB.setOperation(OperationLog.CONFIRM_ERROR.getOperationLogCode());
            System.out.println(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_ERROR.getOperationLogCode()));
            throw new RuntimeException(OperationConstant.operationLogMap.get(OperationLog.CONFIRM_ERROR.getOperationLogCode()));
        } finally {
            //logDAO.save(logDB);
        }
    }
}
