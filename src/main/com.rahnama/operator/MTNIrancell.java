package com.rahnama.operator;

import com.rahnama.bo.ChargeBO;
import com.rahnama.bo.PaymentBO;
import com.rahnama.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * Created by rahnema on 7/11/2017.
 */
@Service(value = "MTNIrancell")
public class MTNIrancell implements Operator {

    public MTNIrancell() {
    }

    @Autowired
   PSP psp;

   public PaymentBO topUP(String phone, String amount, String requestNumber) {
       ChargeBO chargeBO = new ChargeBO(phone,"MTN",amount,requestNumber);
       try {
           return psp.confirm(chargeBO);
       }catch(RuntimeException e){
           return psp.reverse(chargeBO);
       }
   }
}
