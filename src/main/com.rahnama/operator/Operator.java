package com.rahnama.operator;

import com.rahnama.bo.PaymentBO;

/**
 * Created by rahnema on 7/11/2017.
 */
public interface Operator {
    public PaymentBO topUP(String phone, String amount, String requestNumber);
}
