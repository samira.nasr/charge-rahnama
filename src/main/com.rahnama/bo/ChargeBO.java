package com.rahnama.bo;

import com.rahnama.model.Charge;
import com.rahnama.operator.MCI;
import com.rahnama.operator.MTNIrancell;
import com.rahnama.operator.Operator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by rahnema on 7/11/2017.
 */
public class ChargeBO {
    private String phoneNumber;
    private String operatorStr;
    private String amount;
    private String requestNumber;
    private Operator operator;
    private Charge charge;


    public ChargeBO() {
    }

    public ChargeBO(String phoneNumber, String operatorStr, String amount, String requestNumber) {
        this.phoneNumber = phoneNumber;
        this.operatorStr = operatorStr;
        this.amount = amount;
        this.requestNumber = requestNumber;
        this.charge = new Charge(phoneNumber,operatorStr,amount);
    }

    public Charge getCharge() {
        return charge;
    }

    public void setCharge(Charge charge) {
        this.charge = charge;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOperatorStr() {
        return operatorStr;
    }

    public void setOperatorStr(String operatorStr) {
        this.operatorStr = operatorStr;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

//    public void setOperator(String operatorStr) {
//        if(operatorStr.equalsIgnoreCase("MTN"))
//            this.operator = new MTNIrancell();
//        else if(operatorStr.equalsIgnoreCase("MCI")) {
//            this.operator = new MCI();
//        }
//    }
}
