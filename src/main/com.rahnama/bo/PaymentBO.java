package com.rahnama.bo;

import java.io.Serializable;

/**
 * Created by rahnema on 7/11/2017.
 */
public class PaymentBO implements Serializable {
    private String requestNumber;
    private String processMsg;

    public String getProcessMsg() {
        return processMsg;
    }

    public void setProcessMsg(String processMsg) {
        this.processMsg = processMsg;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }
}
