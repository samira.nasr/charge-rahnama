package com.rahnama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChargeApp{

    public static void main(String[] args) {
        SpringApplication.run(ChargeApp.class, args);
    }



}